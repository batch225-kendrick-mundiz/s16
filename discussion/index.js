// Basic Assignment Operator (=)
// The assignment operator adds the value of the right operand to a variable and assigns the result to the variable

let assignmentNumber = 8;
console.log(assignmentNumber);

let totalNumber = assignmentNumber + 2;
console.log("Result of addition assignment operator: " + totalNumber);

/*- When multiple operators are applied in a single statement, it follows the PEMDAS (Parenthesis, Exponents, Multiplication, Division, Addition and Subtraction) rule
            - The operations were done in the following order:
                1. 3 * 4 = 12
                2. 12 / 5 = 2.4
                3. 1 + 2 = 3
                4. 3 - 2.4 = 0.6*/

let mDas = 1 + 2 - 3 * 4 / 5;
console.log("Result of mDas operation: "+mDas);


let juan = "juan";
//equality operator (==)
console.log(1 == 1);
console.log(1 == 2);
console.log(1 =="1");
console.log(0 == false);
console.log("juan" == "juan");
console.log("juan" == juan);

//inequality operator (!=)
console.log(1 != 1);
console.log(1 != 2);
console.log(1 !="1");
console.log(0 != false);
console.log("juan" != "juan");
console.log("juan" != juan);

//strict equality operator (===)
console.log(1 === 1);
console.log(1 === 2);
console.log(1 === "1");
console.log(0 === false);
console.log("juan" === "juan");
console.log("juan" === juan);

//Some comparison operators check whether one value is greater or less than to the other value.

        let a = 50;
        let b = 65;

        //GT or Greater Than operator ( > )
        let isGreaterThan = a > b; // 50 > 65
        //LT or Less Than operator ( < )
        let isLessThan = a < b; // 50 < 65
        //GTE or Greater Than Or Equal operator ( >= ) 
        let isGTorEqual = a >= b; // 50 >= 65
        //LTE or Less Than Or Equal operator ( <= ) 
        let isLTorEqual = a <= b; // 50 <= 65

        //Like our equality comparison operators, relational operators also return boolean which we can save in a variable or use in a conditional statement.
        console.log(isGreaterThan);
        console.log(isLessThan);
        console.log(isGTorEqual);
        console.log(isLTorEqual)

        // Logical Or Operator (|| - Double Pipe)
    // Returns true if one of the operands are true 

    // 1 || 1 = true
    // 1 || 0 = true
    // 0 || 1 = true
    // 0 || 0 = false

    let someRequirementsMet = isLegalAge || isRegistered; 
    console.log("Result of logical OR Operator: " + someRequirementsMet);

    // Logical Not Operator (! - Exclamation Point)
    // Returns the opposite value 
    let someRequirementsNotMet = !isRegistered;
    console.log("Result of logical NOT Operator: " + someRequirementsNotMet);