/*
	1. Debug the following code to return the correct value and mimic the output.
*/

	let num1 = 25;
	let num2 = 5;
	console.log("The result of num1 + num2 should be 30.");
	console.log("Actual Result:");
	console.log(num1 + num2);

	let num3 = 156;
	let num4 = 44;
	console.log("The result of num3 + num4 should be 200.");
	console.log("Actual Result:");
	console.log(num3 + num4);

	let num5 = 17;
	let num6 = 10;
	console.log("The result of num5 - num6 should be 7.");
	console.log("Actual Result:");
	console.log(num5-num6);

	
// 2. Given the values below, calculate the total number of minutes in a year and save the result in a variable called resultMinutes.


	let minutesHour = 60;
	let hoursDay = 24;
	let daysWeek = 7;
	let weeksMonth = 4;
	let monthsYear = 12;
	let daysYear = 365;


	let resultMinutes = minutesHour * hoursDay * daysYear;
	console.log("Minutes in a year: ");
	console.log(resultMinutes);


/*
	3. Given the values below, calculate and convert the temperature from celsius to fahrenheit and save the result in a variable called resultFahrenheit.
*/
	let tempCelsius = 132;

	let resultFahrenheit = (tempCelsius * 1.8) + 32;
	console.log(tempCelsius + " degrees celcius is equal to " +resultFahrenheit+ " degrees fahrenheit");

